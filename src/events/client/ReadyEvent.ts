import { Listener } from "discord-akairo";
import { TextChannel, Message } from "discord.js";

export default class ReadyEvent extends Listener {
    public constructor() {
        super("ready", {
            emitter: "client",
            event: "ready",
            category: "client"
        });
    }

    public exec(): void {
        console.log(`${this.client.user.tag}(${this.client.user.id}) is Ready!`);

        this.client.user.setActivity("Development Mode");
    }
}