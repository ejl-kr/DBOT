import { Command } from "discord-akairo";
import { Message, MessageEmbed, MessageReaction, User } from "discord.js";
import { inspect } from "util";

export default class Eval extends Command {
    public constructor() {
        super("eval", {
            aliases: ["eval", "이발", "이벨"],
            args: [
                {
                    id: "script",
                    type: "string",
                    match: "rest",
                    prompt: {
                        start: "⚠ **실행 할 코드를 입력하세요.**"
                    }
                }
            ],
            ownerOnly: true
        });
    }

    public async exec(message: Message, { script }: { script: string }): Promise<void> {
        const input = script.replace(/^```(js|javascript|jsx|ts|typescript)?\s/, "").replace(/\s?```$/, "");
        const embed = new MessageEmbed().setTitle("코드 실행").setColor("GREEN");

        let type: string | (() => void);

        new Promise(resolve => resolve(eval(input))).then(async (res: any): Promise<void> => {
            let output: string = type = res;

            if (typeof output !== "string") output = inspect(output, { depth: 0 });
            if (typeof type === "function") output = type.toString();
            if (output.includes(<string>this.client.token)) output = output.replace(new RegExp(<string>this.client.token, "gi"), "SECRET");

            if (output.length > 1500) output = `${output.substr(0, 1450)}..`;
            if (!output) output = "결과가 없어요.";

            const m = await message.channel.send(embed.setDescription(`\`\`\`${output}\`\`\``));
        }).catch(async (err: Error) => {
            const m = await message.channel.send(embed.setColor(0xFF0000).setDescription(`\`\`\`${err}\`\`\``));
        });
    }
}