import { Command } from "discord-akairo";
import { Message, MessageEmbed } from "discord.js";

export default class Ping extends Command {
    public constructor() {
        super("ping", {
            aliases: ["핑", "ping", "vld"]
        });
    }

    public async exec(message: Message): Promise<void> {
        const m = await message.channel.send("Pinging..");
        await m.edit("", new MessageEmbed().setDescription("🏓 **퐁!**").addField("Discord API", `> ${this.client.ws.ping} ms`).addField("Message Latency", `> ${m.createdTimestamp - message.createdTimestamp} ms`).setColor("GREEN"));
    }
}